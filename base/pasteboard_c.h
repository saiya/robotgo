#include "pasteboard.h"
#include "os.h"

#if defined(IS_MACOSX)
//	#include "png_io.h"
	#include <ApplicationServices/ApplicationServices.h>
#elif defined(IS_WINDOWS)
	#include "bmp_io.h"
#endif

const char *MMPasteErrorString(MMPasteError err)
{
	switch (err) {
		case kMMPasteOpenError:
			return "Could not open pasteboard";
		case kMMPasteClearError:
			return "Could not clear pasteboard";
		case kMMPasteDataError:
			return "Could not create image data from bitmap";
		case kMMPastePasteError:
			return "Could not paste data";
		case kMMPasteUnsupportedError:
			return "Unsupported platform";
		default:
			return NULL;
	}
}
