#pragma once
#ifndef STR_IO_H
#define STR_IO_H

#include "MMBitmap.h"
#include "io.h"
#include <stdint.h>


enum _MMBMPStringError {
	kMMBMPStringGenericError = 0,
	kMMBMPStringInvalidHeaderError,
	kMMBMPStringDecodeError,
	kMMBMPStringDecompressError,
	kMMBMPStringSizeError, /* Size does not match header. */
	MMMBMPStringEncodeError,
	kMMBMPStringCompressError
};

typedef MMIOError MMBMPStringError;


/* Returns description of given error code.
 * Returned string is constant and hence should not be freed. */
const char *MMBitmapStringErrorString(MMBMPStringError err);

#endif /* STR_IO_H */
